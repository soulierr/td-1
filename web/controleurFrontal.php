<?php

////////////////////
// Initialisation //
////////////////////
require_once __DIR__ . '/../vendor/autoload.php';

/////////////
// Routage //
/////////////

// Syntaxe alternative
// The null coalescing operator returns its first operand if it exists and is not null
TheFeed\Controleur\RouteurURL::traiterRequete();
