<?php

namespace TheFeed\Controleur;

use JetBrains\PhpStorm\NoReturn;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use TheFeed\Lib\Conteneur;
use TheFeed\Lib\MessageFlash;

class ControleurGenerique {

    protected static function afficherVue(string $cheminVue, array $parametres = []): Response
    {
        extract($parametres);
        $messagesFlash = MessageFlash::lireTousMessages();
        ob_start();
        require __DIR__ . "/../vue/$cheminVue";
        $corpsReponse = ob_get_clean();
        return new Response($corpsReponse);
    }

    // https://stackoverflow.com/questions/768431/how-do-i-make-a-redirect-in-php
    protected static function rediriger(string $route, string $controleur = "", string $action = "", array $query = [], array $params = []  ) : RedirectResponse
    {
        Conteneur::recupererService($route);
        $queryString = [];
        if ($action != "") {
            $queryString[] = "action=" . rawurlencode($action);
        }
        if ($controleur != "") {
            $queryString[] = "controleur=" . rawurlencode($controleur);
        }
        foreach ($query as $name => $value) {
            $name = rawurlencode($name);
            $value = rawurlencode($value);
            $queryString[] = "$name=$value";
        }
        $url = "Location: ./controleurFrontal.php?" . join("&", $queryString);
        $redirectResponse = new RedirectResponse($url);
        $redirectResponse->send();
        exit();
    }

    public static function afficherErreur($messageErreur = "", $controleur = ""): void
    {
        $messageErreurVue = "Problème";
        if ($controleur !== "")
            $messageErreurVue .= " avec le contrôleur $controleur";
        if ($messageErreur !== "")
            $messageErreurVue .= " : $messageErreur";

        ControleurGenerique::afficherVue('vueGenerale.php', [
            "pagetitle" => "Problème",
            "cheminVueBody" => "erreur.php",
            "errorMessage" => $messageErreurVue
        ]);
    }

}